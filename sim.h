/* Copyright 2017 Ricardo Meneghin Filho

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software
and associated documentation files (the "Software"), to deal in the Software
without
restriction, including without limitation the rights to use, copy, modify,
merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "./pista.h"
#include "./array_list.h"
#include "./linked_list.h"
#include "./event.h"
#include "./semaphore.h"
#include <iostream>

#include<stdlib.h>

namespace structures{


class CarSimulation {
 public:
   CarSimulation(){
   //adiciona os primeiros carros
   srand(time(NULL));

   // as ruas do topo
   Road* N1sul = new Road{"N1sul", 500.0f, 60.0f, events};
   EndRoad* N1norte = new EndRoad{"N1norte", 500.0f, 60, events};
   Road* N2sul = new Road{"N2sul", 500.0f, 40, events};
   EndRoad* N2norte = new EndRoad{"N2norte", 500.0f, 40, events};
   //ruas do meio
   EndRoad* O1oeste = new EndRoad{"O1oeste", 2000.0f, 80, events};
   Road* C1oeste = new Road{"C1oeste", 300.0f, 60, events};
   Road* L1oeste = new Road{"L1oeste", 400.0f, 30, events};
   Road* O1leste = new Road{"O1leste", 2000.0f, 80, events};
   Road* C1leste = new Road{"C1leste", 3000.0f, 60, events};
   EndRoad* L1leste = new EndRoad{"L1leste", 400.0f, 40, events};

   //ruas de baixo
   EndRoad* S1sul = new EndRoad{"S1sul", 500.0f, 60, events};
   Road* S1norte = new Road{"S1norte", 500.0f, 60, events};
   EndRoad* S2sul = new EndRoad{"S2sul", 500.0f, 40, events};
   Road* S2norte = new Road{"S2norte", 500.0f, 40, events};

   N1sul->set_roads( nullptr, C1leste, S1sul, O1oeste, 80, 10);
   N1norte->set_roads(S1norte);
   N2sul->set_roads(nullptr, L1leste, S2sul, C1oeste, 40 , 30);
   N2norte->set_roads(S2norte);

   O1oeste->set_roads(C1oeste);
   C1oeste->set_roads(L1oeste, S1sul, O1oeste, N1norte, 30, 40);
   L1oeste->set_roads(nullptr, S2sul, C1oeste, N2norte, 30, 30);

   O1leste->set_roads(nullptr, N1norte, C1leste, S1sul, 10, 80);
   C1leste->set_roads(O1leste, N2norte, L1leste, S2sul, 30, 40);
   L1leste->set_roads(C1leste);

   S1sul->set_roads(N1sul);
   S1norte->set_roads(nullptr, O1oeste, N1norte, C1leste, 10, 10);
   S2sul->set_roads(N2sul);
   S2norte->set_roads(nullptr, C1oeste, N2norte, L1leste, 30, 30);
   float time_open1 = 10;
   float time_open2 = 10;
   float time_open3 = 10;
   float time_open4 = 10;
   Semaphore* st1 = new Semaphore{nullptr, N1sul, time_open1};
   Semaphore* sr1= new Semaphore{st1, C1oeste, time_open2};
   Semaphore* sS1sul= new Semaphore{sr1, S1norte, time_open3};
   Semaphore* sl1= new Semaphore{sS1sul, O1leste, time_open4};
   st1->to_right = sl1;
   Semaphore* sN1norte = new Semaphore{nullptr, N2sul, time_open1};
   Semaphore* sr2 = new Semaphore{sN1norte, L1oeste, time_open2};
   Semaphore* sS1norte = new Semaphore{sr2, S2norte, time_open3};
   Semaphore* sl2 = new Semaphore{sS1norte, C1leste, time_open4};
   sN1norte->to_right = sl2;
   //insere os eventos iniciais do semaforo
   Event e2{0, 3, sl2->road};
   e2.semaphore = sl2;
   events->insert_sorted(e2);
   Event e{0, 3, sl1->road};
   e.semaphore = sl1;
   events->insert_sorted(e);
   sources.push_back(N1sul);
   sources.push_back(N2sul);
   sources.push_back(S1norte);
   sources.push_back(S2norte);
   sources.push_back(O1leste);
   sources.push_back(L1oeste);
   roads.push_back(N1sul);
   roads.push_back(N1norte);
   roads.push_back(N2sul);
   roads.push_back(N2norte);
   roads.push_back(O1oeste);
   roads.push_back(C1oeste);
   roads.push_back(L1oeste);
   roads.push_back(O1leste);
   roads.push_back(C1leste);
   roads.push_back(L1leste);
   roads.push_back(S1sul);
   roads.push_back(S1norte);
   roads.push_back(S2sul);
   roads.push_back(S2norte);
   for (int i = 0; i<sources.size();++i){
     Event e2{(float)(10+4*(0.5-rand()/(float)RAND_MAX)), 0, sources[i]};
     events->insert_sorted(e2);
   }
 }

 void print(){
   int total_num_cars = 0;
   std::cout << "e: entrada s: saída p: permanecem\n";
   for (int i = 0; i<roads.size();++i){
     total_num_cars += roads[i]->total_num_cars_left();
     std::cout << roads[i]->nome<<":";
     printf(" e: %d, s: %d, p: %d \n",  roads[i]->total_num_cars_entered(), roads[i]->total_num_cars_left(), roads[i]->total_num_cars_on_road());

   }
 }

  void step(){
    //olha o proximo evento
    if (!events->empty()){
      Event e = events->at(0);
      now_ = e.time_;
      //printf("stepping type: %d time: %f\n", e.type_, e.time_);
      switch (e.type_){
        case 0:{//novo carro entrando na pista
          Car car{(float)(2 +3*rand()/(float)RAND_MAX)};
          e.source_->add(car, e.time_);
          Event e2{e.time_+(float)(10+4*(0.5-rand()/(float)RAND_MAX)), 0, e.source_};
          events->insert_sorted(e2);
          //printf("new car event time:%f\n", e2.time_);
          break;
        }
        case 1:{//chegada do carro ao semaforo
          e.source_->road_change(e.time_);
          break;
        }
        case 2:{//troca de pista
           e.source_->road_change(e.time_);
           break;
         }
        case 3:{//semaforo abrindo
          e.source_->open_semaphore();
          auto a = e.time_;
          e.source_->road_change(a);//inicia uma nova onda de eventos de carros saindo
          //cria um novo evento para quando o próximo semaforo abrir
          //printf("new event time:%f\n", e.time_+e.semaphore->time_open);
          Event e2{e.time_+e.semaphore->time_open, 3, e.semaphore->to_right->road};
          e2.semaphore = e.semaphore->to_right;
          events->insert_sorted(e2);
          //cria um novo evento para quando fechar
          Event e3{e.time_+e.semaphore->time_open, 4, e.source_};
          events->insert_sorted(e3);
          break;
        }
        case 4:{//semaforo fechando
          e.source_->close_semaphore();
          break;
        }
    }
    events->pop_front();
  }
 }

 float now(){
   return now_;
 }

 private:
  float now_ = 0;
  ArrayList<Road*> sources{10};
  ArrayList<Road*> roads{14};
  LinkedList<Event>* events = new LinkedList<Event>{};
};

}
