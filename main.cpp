#include "./sim.h"


int main(){
    //printf("entrando no main\n");
    //fflush(stdout);
    using namespace std;
    structures::CarSimulation sim{};
    clock_t begin = clock();
    int i =0;
    int n_sec = 1e6;
    while (sim.now()<n_sec){
       sim.step();
       i++;
    }
    sim.print();
    clock_t end = clock();
    double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC*1000;
    std::cout << "Simulated "<< n_sec << " seconds in " << elapsed_secs<<" ms\n";
}
