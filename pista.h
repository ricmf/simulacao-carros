/* Copyright 2017 Ricardo Meneghin Filho

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software
and associated documentation files (the "Software"), to deal in the Software
without
restriction, including without limitation the rights to use, copy, modify,
merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#pragma once

#include <cstdint>
#include <stdexcept>
#include "./carro.h"
#include "./array_queue.h"
#include "./array_list.h"
#include "./linked_list.h"
#include "./event.h"

namespace structures {
class Road {
 public:

   Road(std::string nome_, float length_, float speed_, LinkedList<Event>* events_) : length(length_), speed(speed_/3.6), events(events_), nome(nome_){}

   void set_roads(Road* road_on_back_, Road* left, Road* top, Road* right, int perc_left, int perc_front){
     roads.push_back(left);
     roads.push_back(top);
     roads.push_back(right);
     road_on_back = road_on_back_;
     first_tertile = perc_left/100.0f;
     second_tertile = first_tertile + perc_front/100.0f;
     decide_next_road();
   }

   bool semaphore_open{false};

   void close_semaphore(){
     semaphore_open = false;
   }

   void open_semaphore(){
     semaphore_open = true;
   }

  virtual void add(const Car& car, float now){
    if (has_space(car)){
      cars_entered++;
      if (cars.empty()){
        Event e{now+length/speed, 1, this};// esse evento é a chegada do carro ao semáforo
        events->insert_sorted(e);
      }
      cars.enqueue(car);
      entries.enqueue(now);
      queue_end += car.length_ + 3;
    }
  }

  virtual void road_change(float now){
    //printf("road_change1 cars size is %lu speed is %f length is %f now is %f semaphore is %d: \n", cars.size(), speed, length, now, semaphore_open);
    fflush(stdout);
    if (!cars.empty() && semaphore_open){
      if (now<length/speed + entries.front()){ // se o carro ainda não chegou no fim adiciona o evento de chegada dele ao fim
        //printf("  Entries.front():%f now: %f\n", entries.front(), now);
        Event e{length/speed + entries.front(), 1, this}; // esse evento é a chegada do carro ao semáforo
        events->insert_sorted(e);
        //printf("    new event %f \n", e.time_);
      } else if (next_road->has_space(cars.front())){
          //printf("  changing roads\n");
          auto car = cars.dequeue();
          cars_left++;
          entries.dequeue();
          queue_end -= car.length_ + 3;
          next_road->add(car, now);
          decide_next_road();
          if (road_on_back != nullptr){
              road_on_back->road_change(now);
          }
          float time =now+(car.length_+3)/speed;
          Event e{time, 1, this};// esse evento é a chegada do carro ao semáforo
          events->insert_sorted(e);
          //printf("    new semaphore arrival event %f \n", e.time_);
      } else {
          //printf("  Road has no space\n");
      }
    }

  }

  int total_num_cars_entered(){
    return cars_entered;
  }

  int total_num_cars_left(){
    return cars_left;
  }

  int total_num_cars_on_road(){
    return cars_entered - cars_left;
  }

  bool has_space(const Car& car){
    //printf("queue end: %f car len %f length %f, so has_space = %d \n", queue_end, car.length_, length, queue_end + car.length_ + 3 <= length);
    return queue_end + car.length_ + 3 <= length;
  }

  ArrayQueue<Car> cars{2000u};
  ArrayQueue<float> entries{2000u};
  ArrayList<Road*> roads{3};
  LinkedList<Event> *events{nullptr};
  float length;
  float speed;
  int cars_entered{0};
  int cars_left{0};
  std::string nome{""};


 private:
  Road* road_on_back{nullptr};
  float queue_end{0};
  Road* next_road{nullptr};
  float first_tertile = 0.0;
  float second_tertile = 0.0;
  void decide_next_road() {
    float f = rand()/(float)RAND_MAX;
    //printf("%f\n",f );
    if (f<first_tertile){
      next_road = roads[0];
    } else if (f<second_tertile){
      next_road = roads[1];
    } else {
      next_road = roads[2];
    }
  }

};

class EndRoad : public Road {
 public:
   EndRoad(std::string nome_, float length_, float speed_, LinkedList<Event>* events) : Road(nome_, length_, speed_, events){};


   void set_roads(Road* road_on_back_){
     Road::set_roads(road_on_back_, nullptr, nullptr, nullptr, 0, 0);
   }

   void add(const Car& car, float now){
      Road::cars_entered++;
      Event e{now+length/speed, 1, this};// esse evento é a chegada do carro ao semáforo
      Road::events->insert_sorted(e);
      Road::cars.enqueue(car);
      Road::entries.enqueue(now);
    }

    void road_change(float now){
      if (!cars.empty()){
        Road::cars_left++;
        Road::cars.dequeue();
        Road::entries.dequeue();
      }
    }
};
}  // namespace structures
