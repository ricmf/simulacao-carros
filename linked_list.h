/* Copyright 2017 Ricardo Meneghin Filho

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software
and associated documentation files (the "Software"), to deal in the Software
without
restriction, including without limitation the rights to use, copy, modify,
merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef STRUCTURES_LINKED_LIST_H
#define STRUCTURES_LINKED_LIST_H

#include <cstdint>
#include <stdexcept>

namespace structures {
class Node;
template <typename T>
/** a list in which every element has a pointer to the next one.
      */
class LinkedList {
 public:
 /** a node in the list, which contains the data and pointer to the next node
      */
  class Node {  // Elemento
   public:
 /** build a node with the given data and a pointing to null
      */
    explicit Node(const T& data) : data_{data} {}
/** builds a node with the given data and pointer
      */
    Node(const T& data, Node* next) : data_{data}, next_{next} {}
/** returns the data of this node
      */
    T& data() {  // getter: dado
      return data_;
    }
/** returns the data of this node
      */
    const T& data() const {  // getter const: dado
      return data_;
    }

/** a pointer to the next node
      */
    Node* next() {  // getter: próximo
      return next_;
    }

/** a pointer to the next node
      */
    const Node* next() const {  // getter const: próximo
      return next_;
    }

/** sets the next node
      */
    void next(Node* node) {  // setter: próximo
      next_ = node;
    }

   private:
    T data_;
    Node* next_{nullptr};
  };

  /** builds an empty list
        */
  LinkedList() {
    head = nullptr;
    size_ = 0;
  }                 // construtor padrão
  ~LinkedList() {clear();}  // destrutor

  /** clears the list
        */
  void clear() {
    while (!empty()) {
      pop_front();
    }
  }  // limpar lista

  /** pushes an element to the back of the list
        */
  void push_back(const T& data) {
    if (empty()) {
      head = new Node{data};
    } else {
      auto node = head;
      while (node->next() != nullptr) {
        node = node->next();
      }
      auto p = new Node{data};
      node->next(p);
    }
    size_++;
  }  // inserir no fim

  /** pushes an element in the front  of the list
        */
  void push_front(const T& data) {
    head = new Node(data, head);
    size_++;
  }  // inserir no início

  /** inserts at the given index
        */
  void insert(const T& data, std::size_t index) {
    auto node = nodeAt(index - 1);
    node->next(new Node(data, node->next()));
    size_++;
  }  // inserir na posição

  /** inserts after the first item smaller than data
        */
  void insert_sorted(const T& data) {
      auto node = head;
      auto last = head;
      while (node != nullptr) {
	      if (node->data() > data) {
             break;
      	}
        last = node;
        node = node->next();
      }
      if (node == head) {
        head = new Node{data, head};
      } else {
        last->next(new Node{data, node});
      }
    size_++;
  }  // inserir em ordem

  /** returns the node at the given index
        */
  Node* nodeAt(std::size_t index) {
    if (index >= size_) {
      throw std::out_of_range("index too big");
    }
    auto node = head;
    for (int i = 0; i < index; ++i) {
      node = node->next();
    }
    return node;
  }  // acessar um elemento na posição index

  /** returns the data at the given index
        */
  T& at(std::size_t index) {
    return nodeAt(index)->data();
  }  // acessar um elemento na posição index

  /** removes and returns the item in the given index
        */
  T pop(std::size_t index) {
    if (empty() || index >= size_) {
      throw std::out_of_range("invalid index");
    }
    if (index == 0) {
      return pop_front();
    } else {
      auto node = nodeAt(index - 1);
      auto val = node->next()->data();
      auto old = node->next();
      node->next(node->next()->next());
      delete old;
      size_--;
      return val;
    }
  }  // retirar da posição

  /** pops last item
        */
  T pop_back() { return pop(size_ - 1); }  // retirar do fim

  /** pops first item
        */
  T pop_front() {
    if (empty()) {
      throw std::out_of_range("empty list");
    }
    auto val = head->data();
    if (size_ == 1) {
      auto old = head;
      head = nullptr;
      //delete old;
    } else {
      auto old = head;
      head = head->next();
      //delete old;
    }
    size_--;
    return val;
  }  // retirar do início

  /** removes the given item if it exists
        */
  void remove(const T& data) {
    if (empty()) {
      return;
    }
    auto node = head;
    auto last = head;
    while (node->data() != data && node->next() != nullptr) {
      last = node;
      node = node->next();
    }
    if (node->next() == nullptr) {
      return;
    }
    last->next(node->next());
    delete node;
    size_--;
  }  // remover específico

  /** is the list empty?
        */
  bool empty() const { return head == nullptr; }  // lista vazia

  /** does the list contain the given item?
        */
  bool contains(const T& data) const {
    auto node = head;
    while (node != nullptr) {
      if (node->data() == data) {
        return true;
      }
      node = node->next();
    }
    return false;
  }  // contém

  /** returns the index of the first appearance of the item if it exists, size() otherwise
        */
  std::size_t find(const T& data) const {
    auto node = head;
    int i = 0;
    while (node != nullptr) {
      if (node->data() == data) {
        return i;
      }
      ++i;
      node = node->next();
    }
    return size_;
  }  // posição do dado

  /** returns the size of the list
        */
  std::size_t size() const { return size_; }  // tamanho da lista

 private:
  Node* end() {  // último nodo da lista
    auto it = head;
    for (auto i = 1u; i < size(); ++i) {
      it = it->next();
    }
    return it;
  }

  Node* head{nullptr};
  std::size_t size_{0u};
};
}  // namespace structures

#endif
