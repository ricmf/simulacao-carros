/* Copyright 2017 Ricardo Meneghin Filho

Permission is hereby granted, free of charge, to any person obtaining a copy of this software
and associated documentation files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/



#ifndef STRUCTURES_ARRAY_QUEUE_H
#define STRUCTURES_ARRAY_QUEUE_H

#include <cstdint>    // std::size_t
#include <stdexcept>  // C++ Exceptions

namespace structures {

template <typename T>
/** A implementation of a cyclic buffer of fixed maximum size, obeying the FIFO rule.
      */
class ArrayQueue {
 public:
  ArrayQueue() {
  auto max = 10;
  max_size_ = max;
  max_end = max + 1;
  contents = new T[max];
  beg_ = 0;
  end_ = 0;
  size_ = 0;
 }

    /** instantiates a new ArrayQueue with the supplied maximum size
      */
  explicit ArrayQueue(std::size_t max) {
    max_size_ = max;
    max_end = max + 1;
    contents = new T[max];
    beg_ = 0;
    end_ = 0;
    size_ = 0;
  }

  ~ArrayQueue() { delete[] contents; }

  /** adds the given element to the queue
      */
  void enqueue(const T& data) {
    if (full()) {
      throw std::out_of_range("queue is full");
    }
    if (end_ == max_size_) {
      contents[0] = data;
      end_ = 1;
    } else {
      contents[end_] = data;
      end_ = (end_ + 1) % (max_end);
    }
    size_++;
  }

  /**removes the first element from the queue
         * \return the removed element
      */
  T dequeue() {
    if (empty()) {
      throw std::out_of_range("queue is empty");
    }
    T val = contents[beg_];
    beg_ = (beg_ + 1) % max_size_;
    end_ = end_ % max_size_;
    size_--;
    return val;
  }

  /** returns the last element of the queue, if it is not empty.
         * \return the last element of the queue
      */
  T& back() {
    if (empty()) {
      throw std::out_of_range("queue is empty so theres no back");
    }
    return contents[end_ - 1];
  }

  T& front() {
    if (empty()) {
      throw std::out_of_range("queue is empty so theres no front");
    }
    return contents[beg_];
  }

  /** clears the queue
      */
  void clear() {
    beg_ = 0;
    end_ = 0;
    size_ = 0;
  }

  /** returns the number of elements in the queue
         * \return the number of elements in the queue
      */
  std::size_t size() { return size_; }

  /** returns the maximum number of elements in the queue
         * \return the maximum number of elements in the queue
      */
  std::size_t max_size() { return max_size_; }

  /**
         * \return true if the queue is empty, false otherwise
      */
  bool empty() { return (size() == 0); }

  /**
         * \return true if the queue is full, false otherwise
      */
  bool full() { return (size() == max_size_); }

 private:
  T* contents;

  std::size_t beg_;
  std::size_t end_;
  std::size_t max_size_;
  std::size_t max_end;
  std::size_t size_{0};
  static const auto DEFAULT_SIZE = 10u;
};

};  // namespace structures

#endif
